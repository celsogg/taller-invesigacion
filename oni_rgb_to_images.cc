#include "playback.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <libgen.h> // basename

#include <string>
#include <iomanip>

// dirs
#include <sys/stat.h> 
#include <sys/types.h>

#include <fstream>

void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
   int len = std::max(src.cols, src.rows);
   cv::Point2f pt(src.cols/2., src.rows/2.);
   cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
   cv::warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
}

void printUsage()
{
   printf("\n  Usage: oni_rgb_to_images input_oni_file [output_images_folder] [-r]\n");
   printf("\n   -r: rotate image 180°\n\n");  
}

int main(int argc, char* argv[])
{
   
   Playback playback = Playback();
   char oni_filename[1000] = "";
   char folder[1000] = "";
   int rotate_flag = 0, folder_flag = 0;

   if(argc < 2) {
      printUsage();
      return -1;
   }

   if (argc == 2 && strcmp(argv[1], "-h") == 0)
   {
      printUsage();
      return 0;
   }

   if (argc > 2 && strcmp(argv[2], "-r") != 0)
   {
      strcpy(folder, argv[2]);
      folder_flag = 1;
   }
   
   for (int i = 1; i < argc; ++i)
   {
      if (strcmp(argv[i], "-r") == 0)
      {
         rotate_flag = 1;
      }
   }

   strcpy( oni_filename, argv[1]);
   playback.init(oni_filename);
   playback.player.SetPlaybackSpeed(XN_PLAYBACK_SPEED_FASTEST);
   printf("  oni   : %s\n", oni_filename);
   
   std::string base;
   if (folder_flag == 1)
   {
      base = folder;
   }
   else
   {
      base = (basename((char *)oni_filename));
   }

   int lastindex = base.find_last_of(".");
   std::string rawname = base.substr(0, lastindex); 

   printf("  folder: %s\n", rawname.c_str());

   std::ofstream out;
   std::string fileViperInfo(rawname);
   fileViperInfo += "/";
   fileViperInfo += rawname ;
   fileViperInfo += ".info" ;
   printf("  viper : %s\n", fileViperInfo.c_str());

   struct stat st = {0};

   if (stat(rawname.c_str(), &st) == -1)
   {
      std::cout << "  creating folder... ";
      int result = mkdir(rawname.c_str(), 0777);
      if (result == 0)
      {
         std::cout << "ok" << std::endl;
      }
      else
      {
         std::cout << "  can't create folder." << std::endl;
         return -1;
      }
   } 
   else
   {
      std::cout << "  folder already exists... ok" << std::endl;
   }

   std::string viperInfo = "#VIPER_VERSION_3.0\n1\n";

   XnUInt32 frames;
   playback.player.GetNumFrames(playback.g_image.GetName(), frames);
   printf("  Frames rgb: %i\n", frames);
   while (playback.update())
   { 
      if(playback.frame%30 == 0)
      {
         printf("  %d %lu\n", playback.frame, playback.timestamp);
      }

      std::stringstream buffer;
      buffer << std::setfill('0') << std::setw(6) << playback.frame ;
      buffer << ".jpg" ;

      viperInfo += rawname + "-" + buffer.str() + "\n";
      std::string filename =  rawname + "/" + rawname + "-" + buffer.str();

      if (rotate_flag)
         rotate(playback.rgb, 180, playback.rgb);
      
      imwrite(filename.c_str(), playback.rgb);
      
      char c = cv::waitKey(5);
      if(c == 'q')
         break;
   }


   out.open(fileViperInfo.c_str());
   if (out.is_open())
   {
      out << viperInfo;
   }
   else
   {
      printf("Error writing file viper .info\n");
   }

   out.close();

   return 0;
}