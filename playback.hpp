#ifndef INCLUDE_NI
#include <XnCppWrapper.h>
#define INCLUDE_NI
#endif

#ifndef INCLUDE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#define INCLUDE_OPENCV
#endif

#include <string>
#include <iostream>

class Playback
{

public:
	xn::Context ni_context;
	xn::Player player;
	xn::DepthGenerator g_depth;
	xn::ImageGenerator g_image;
	void convert_pixel_maps();

	cv::Mat depth, rgb;
	const XnDepthPixel* pDepthMap;
	const XnRGB24Pixel* pImageMap;
	int rows, cols, fps;

	const XnChar* depthname;

	XnUInt32 frame;
	XnUInt64 timestamp;

	Playback();

	void init(const char* oni_filename);
	bool update();
	void seek(int offset);
};
