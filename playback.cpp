#include "playback.hpp"

Playback::Playback(){}

void Playback::convert_pixel_maps()
{
	depth = cv::Mat(rows, cols, CV_16UC1, (void*) pDepthMap);

	rgb = cv::Mat(rows, cols, CV_8UC3);
	cv::MatIterator_<cv::Vec3b> it2 = rgb.begin<cv::Vec3b>();
	
	for (unsigned int i = 0; i < rows; i++) {
		for (unsigned int j = 0; j < cols; j++) {
			(*it2)[0] = (pImageMap[i*cols + j]).nBlue;
			(*it2)[1] = (pImageMap[i*cols + j]).nGreen;
			(*it2)[2] = (pImageMap[i*cols + j]).nRed;
			it2++;
		}
	}
}

void Playback::init(const char* oni_filename)
{
	XnStatus nRetVal = XN_STATUS_OK;

	nRetVal = ni_context.Init();  
	if (nRetVal != XN_STATUS_OK)
	{
		printf("Failed to initialize OpenNI: %s\n", xnGetStatusString(nRetVal));
		exit(-1);
	}

	nRetVal = ni_context.OpenFileRecording(oni_filename, player);
	player.SetRepeat(false);

	if ( nRetVal != XN_STATUS_OK )
	{
		printf("Failed to open node %s\n", oni_filename);
		exit(-1);
	}

	nRetVal = ni_context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_depth);
	if ( nRetVal != XN_STATUS_OK )
	{
		printf("Failed to get depth node: %s\n", xnGetStatusString(nRetVal));
		exit(-1);
	}

	const xn::NodeInfo info = g_depth.GetInfo();
	depthname = info.GetInstanceName();

	nRetVal = ni_context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_image);
	if ( nRetVal != XN_STATUS_OK )
	{
		printf("Failed to get image node: %s\n", xnGetStatusString(nRetVal));
		exit(-1);
	}

	XnMapOutputMode mapMode;
	g_depth.GetMapOutputMode(mapMode);

	rows = mapMode.nYRes;
	cols = mapMode.nXRes;
	fps = mapMode.nFPS;
	rgb = cv::Mat(rows, cols, CV_8UC3);
	depth = cv::Mat(rows, cols, CV_32FC1);
}

bool Playback::update()
{
	if(player.IsEOF())
		return false;

	XnStatus nRetVal = XN_STATUS_OK;  
	nRetVal = g_image.WaitAndUpdateData();
	
	if (nRetVal != XN_STATUS_OK)
		return false;

	nRetVal = g_depth.WaitAndUpdateData();
	
	if (nRetVal != XN_STATUS_OK)
		return false;

	player.TellFrame(depthname, frame);
	player.TellTimestamp(timestamp);

	rgb.release();
	depth.release();

	pDepthMap = g_depth.GetDepthMap();
	pImageMap = g_image.GetRGB24ImageMap();

	convert_pixel_maps();
	
	return true;
}

void Playback::seek(int offset)
{
	player.SeekToFrame(g_depth.GetName(), offset, XN_PLAYER_SEEK_CUR);
	player.SeekToFrame(g_image.GetName(), offset, XN_PLAYER_SEEK_CUR);
}
