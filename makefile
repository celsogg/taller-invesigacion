CXX := g++ -w -ggdb `pkg-config --cflags opencv`
INCLUDES := -I. -I/usr/include/ni -I/usr/local/include -I/opt/local/include
LIBS := -L/usr/local/lib/ -lopencv_video -lopencv_highgui -lopencv_imgproc -lopencv_core -lopencv_videostab -L/usr/lib/ -lOpenNI `pkg-config --libs opencv`

all: playback.o oni_rgb_to_video play play_video oni_rgb_to_images

playback.o: playback.cpp
	$(CXX) playback.cpp -c $(INCLUDES) -o playback.o $(LIBS)

oni_rgb_to_video: oni_rgb_to_video.cc playback.o
	$(CXX) oni_rgb_to_video.cc playback.o $(INCLUDES) -o oni_rgb_to_video $(LIBS)

play: play.cc playback.o
	$(CXX) play.cc playback.o $(INCLUDES) -o play $(LIBS)

play_video: play_video.cc
	$(CXX) play_video.cc $(INCLUDES) -o play_video $(LIBS)

oni_rgb_to_images: oni_rgb_to_images.cc playback.o
	$(CXX) oni_rgb_to_images.cc playback.o $(INCLUDES) -o oni_rgb_to_images $(LIBS)

clean:
	rm -f play play_video playback.o oni_rgb_to_images oni_rgb_to_video
