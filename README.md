Código generado para el taller de investigación.

Para compilar todo simplemente ejecutar make.

- playback: clase para reproducir archivos oni, utiliza openni.

- play: reproduce y muestra imagen más profundidad (en escala de grises) dado un archivo oni.

- oni_rgb_to_video: exporta a un archivo de video .avi la parte rgb de un archivo .oni.

- play_video: reproduce un archivo .avi.

- oni_rgb_to_images: exporta la información rgb de un archivo oni a imágenes y crea un archivo compatible con viper-gt.

Requiere:

  - Sistema operativo Linux
  - Opencv 2.11 compilado con soporte para openni