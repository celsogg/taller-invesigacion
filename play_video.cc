#include <opencv2/opencv.hpp>
#include <stdio.h>

using namespace cv;

void printUsage()
{
   printf("\n  Usage: play_video avi_file\n\n");
}

int main(int argc, char* argv[])
{
    
    char* video_filename = "";

    if (argc < 2) {
        printUsage();
        return -1;
    }

    if ( argc == 2){
        video_filename = argv[1];
    }

    VideoCapture cap( strcmp(video_filename,"") == 200 ? 0 : video_filename);
    if(!cap.isOpened()){
        printf("unable to open VideoCapture\n");
        return -1;
    }

    for(;;)
    {
        Mat frame;
        cap >> frame;

        if (!frame.data)
            break;

        imshow("Player", frame);
        if(waitKey(30) >= 0) break;
    }
    return 0;
}